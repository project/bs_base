CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Misc
 * Goals & Features
 * Documentation
 * Learning resources
 * Maintainers


INTRODUCTION
------------

Base theme with a goal to support full frontend Drupal theme development
workflow from start to finish.


REQUIREMENTS
------------

This theme has no additional requirements.


INSTALLATION
------------

 * Install as you would normally install a contributed Drupal theme. See:
   https://www.drupal.org/docs/8/extending-drupal-8/installing-themes for further information.


CONFIGURATION
-------------

The theme has no custom or new modifiable settings.


Goals & Features
----------------

- Developer oriented - no tons of UI settings or big assumptions.
- Presents and use good development workflow for building themes.
- Light weight.
- BEM oriented.
- Implements bs_bootstrap which is Bootstrap 4 based child theme.
- Gulp build process.
- Semantically clean HTML as much as possible - use mixins and don't hardcode
  CSS framework classes where it does not make sense.
- Explore all new nice&cool stuff that is supported in major browsers.
- Ideally everything should be easy to override.
  Stay flexible and let child theme developers to do what they want how they
  want. Ideally we are just offering standardized workflow and are letting child
  theme developer to reuse everything or just parts.
- Use just the APIs that exist in Drupal core, do not invent new APIs.


Documentation
-------------

For the most up-to-date documentation, please visit https://www.drupal.org/docs/8/themes/bs-base.

For support, visit the #bs_base channel on Slack at https://drupal.slack.com/archives/C07M3C0RATC.


### Install

We need some global dependencies: node, npm, and gulp-cli.

For node and npm it is recommended to use nvm to install and manage different
node versions on your system. For installation of nvm check https://github.com/nvm-sh/nvm#installing-and-updating.

Current version of bs_base requires node 18.

To install gulp-cli execute:
```
$ npm install -g gulp-cli
```

Instead of npm you can use pnpm installer which is much faster and requires
less resources on your system. Check https://pnpm.io/installation for more
details how to install pnpm on your system.

After all dependencies are installed run:
```
$ drush bs-tc bs_bootstrap custom_theme Custom Custom
```

Which will create a new custom_theme from a parent bs_bootstrap theme.


### Standard stuff

In your custom theme execute next commands:

Download all dependencies and build CSS files.
```
$ npm run build-css
```

Build CSS with
```
$ npx gulp sass
```

Use watch task if you like
```
$ npx gulp watch
```

Just build everything for production
```
$ npx gulp
```

Specific Gulp task we currently offers are:

- npx gulp sass
- npx gulp sass:dev
- npx gulp sass:lint
- npx gulp clean:css

Additionally, bs_bootstrap child theme offers next two tasks:

- npx gulp bootstrap:js
- npx gulp bootstrap:js:dev
- npx gulp clean:bootstrap

Watchers:

- npx gulp watch
- npx gulp watch-dev

Run all tasks:

- npx gulp
- npx gulp prod
- npx gulp dev


### gulp-options.json

Various config variables we use for gulp build. Do note that if paths
are relative they are relative from position of gulpfile.js.


### Browser support explained

Defined in `.browserlistsrc` file in theme root folder:

```
# Browsers that we support

# > 0.5%, last 2 versions, Firefox ESR, not dead
defaults
```

More info on https://github.com/browserslist/browserslist.


Learning resources
------------------

- https://github.com/Famolus/awesome-sass
  Curated list of awesome Sass and SCSS frameworks, libraries, style guides,
  articles, and resources.


MAINTAINERS
-----------

Current maintainers:
 * Ivica Puljic (pivica) - https://www.drupal.org/u/pivica

This project has been sponsored by:
 * ACTO Team - https://www.drupal.org/acto-team
   Drupal development, design and support company.
 * MD Systems - https://www.drupal.org/md-systems
   Drupal core and contributed development.
   MD Systems is working for small start-ups as well as large Swiss corporations
   with international, multilingual websites.

