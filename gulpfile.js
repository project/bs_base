/**
 * @file
 * Provides Gulp configurations and tasks for bs_base theme and child themes.
 */

'use strict';

// Load gulp and needed lower level libs.
const gulp = require('gulp');
const yaml = require('js-yaml');
const fs = require('fs');
const sass = require('gulp-sass')(require('node-sass'));

// Load gulp options.
let options = yaml.load(fs.readFileSync('./gulp-options.yml', 'utf8'));

// Lazy load gulp plugins.
// By default, gulp-load-plugins will only load "gulp-*" and "gulp.*" tasks,
// so we need to define additional patterns for other modules we are using.
const plugins = require('gulp-load-plugins')(options.gulpPlugins);

// Load gulp tasks.
require('./gulp-tasks.js')(gulp, sass, plugins, options);
