<?php

namespace Drupal\bs_bootstrap;

use Drupal\Core\Render\Element;
use Drupal\Core\Render\Element\RenderCallbackInterface;

/**
 * Provides a trusted callback to alter form.
 */
class BsBootstrapFormAlter implements RenderCallbackInterface {

  /**
   * Enable floating labels on passed element and all of its children callback.
   */
  public static function enableFloatingLabels($element) {
    // Copy floating enable info to all children elements - needed for
    // bs_bootstrap_preprocess_form_element().
    $elements_enable_floating_labels = function (&$element) use (&$elements_enable_floating_labels) {
      $children = Element::children($element);
      foreach ($children as $key) {
        $element[$key]['#floating_labels'] = TRUE;
        $elements_enable_floating_labels($element[$key]);
      }
    };
    $elements_enable_floating_labels($element);

    return $element;
  }

}
