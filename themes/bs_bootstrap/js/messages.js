/**
 * @file
 * Message template overrides.
 */

((Drupal) => {

  const statusClasses = {
    'status': 'success',
    'error': 'danger',
    'warning': 'warning',
    'info': 'info',
  };

  /**
   * Overrides messageInternalWrapper static function.
   *
   */
  Drupal.Message.messageInternalWrapper = function(messageWrapper) {
    const innerWrapper = document.createElement('div');
    innerWrapper.setAttribute('class', 'alerts__wrapper');
    messageWrapper.insertAdjacentElement('afterbegin', innerWrapper);
    return innerWrapper;
  }

  /**
   * Overrides message theme function.
   *
   * @param {object} message
   *   The message object.
   * @param {string} message.text
   *   The message text.
   * @param {object} options
   *   The message context.
   * @param {string} options.type
   *   The message type.
   * @param {string} options.id
   *   ID of the message, for reference.
   *
   * @return {HTMLElement}
   *   A DOM Node.
   */
  Drupal.theme.message = ({ text }, { type, id }) => {
    const messagesTypes = Drupal.Message.getMessageTypeLabels();
    const messageWrapper = document.createElement('div');

    messageWrapper.setAttribute(
      'class',
      `alert alert-${statusClasses[type]} alert-dismissible fade show`,
    );
    messageWrapper.setAttribute(
      'role',
      type === 'error' || type === 'warning' ? 'alert' : 'status',
    );
    messageWrapper.setAttribute('aria-labelledby', `${id}-title`);
    messageWrapper.setAttribute('data-drupal-message-id', id);
    messageWrapper.setAttribute('data-drupal-message-type', type);

    messageWrapper.innerHTML = `
      <button type="button" class="close" data-dismiss="alert" aria-label="${Drupal.t('Close')}">
        <span aria-hidden="true">&times;</span>
      </button>
      <h2 id="${id}-title" class="sr-only">
        ${messagesTypes[type]}
      </h2>
      ${text}
    `;

    return messageWrapper;
  };
})(Drupal);
