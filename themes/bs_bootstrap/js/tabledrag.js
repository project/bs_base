/**
 * @file
 * Table dragging overrides for Bootstrap theme.
 */

(function ($, Drupal) {

  'use strict';

  $.extend(Drupal.theme, /** @lends Drupal.theme */{

    /**
     * @return {string}
     *   Markup for the warning in Bootstrap style.
     */
    tableDragChangedWarning: function () {
      return '<div class="tabledrag-changed-warning alert alert-warning" role="alert">' + Drupal.theme('tableDragChangedMarker') + ' ' + Drupal.t('You have unsaved changes.') + '</div>';
    }
  });

})(jQuery, Drupal);
