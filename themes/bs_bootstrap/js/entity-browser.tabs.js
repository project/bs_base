/**
 * @file
 *
 * Use Bootstrap tabs for Entity Browser tabs.
 */

(function ($, Drupal) {

  'use strict';

  /**
   * Theme override function for entity browser tabs.
   *
   * @return {object}
   *   This function returns a jQuery object.
   */
  Drupal.theme.entityTabs = function () {
    return $('<ul class="nav nav-tabs" role="navigation" aria-label="Tabs"></ul>');
  };

  /**
   * Theme override function for an entity browser tab.
   *
   * @param {object} settings
   *   An object with the following keys:
   * @param {string} settings.title
   *   The name of the tab.
   * @param {string} settings.class
   *   Classes for the tab.
   * @param {string} settings.id
   *   ID for the data- button ID.
   *
   * @return {object}
   *   This function returns a jQuery object.
   */
  Drupal.theme.entityTab = function (settings) {
    return $('<li class="nav-item" tabindex="-1"></li>')
      .addClass(settings.class)
      .append($('<a class="nav-link" href="#"></a>').addClass(settings.class).attr('data-button-id', settings.id)
        .append(settings.title)
      );
  };

})(jQuery, Drupal);
