CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Misc



INTRODUCTION
------------

This is a bootstrap base theme based on bs_base parent theme.

Extend this theme if you are making child theme based on Bootstrap CSS
framework. Instead of manually extending this theme you can use provided
starterkit_bootstrap template.



REQUIREMENTS
------------

This theme requires the following modules:

 * BS Lib (https://drupal.org/project/bs_lib)



INSTALLATION
------------

 * Install and enable bs_lib module first. See [Installing Drupal 8 Modules](https://www.drupal.org/docs/8/extending-drupal-8/installing-drupal-8-modules)
   page for further information.
 * Install as you would normally install a contributed Drupal theme. See
   [Installing Themes](https://www.drupal.org/docs/8/extending-drupal-8/installing-themes)
   for further information.



CONFIGURATION
-------------

This theme supports various configuration options on the theme settings page in
the 'BS Bootstrap' section.


## Navigation bar toggler

In order to use [Bootstrap navigation bar toggler](https://getbootstrap.com/docs/4.0/components/navbar/#toggler)
you need to add 'Navigation bar toggler' block to some region. Usually you will
add this block to 'Navigation Second' region, but it is possible to use some
other region and tweak CSS layout rules if needed. However you can not add this
block to 'Navigation Bar' region because this is the region which toggle button
will collapse/expand by default. 


## Navigation Bar menu

All menu blocks that are added to 'Navigation Bar' region will be converted to
[Navbar Bootstrap control](https://getbootstrap.com/docs/4.0/components/navbar/).
This is the only region that currently support navbar control.


## Header navigation container types

For the header top and navigation bar header type we are supporting two
container types fluid (default) and fixed. You can change this option on  theme
settings page.

## Navbar types

This theme supports two navigation bar types in theme settings option:
- 'Second level dropdown', is good when you need just two menu levels where 1st
  menu level will be ordinary navigation items layout horizontally and 2nd menu
  level items will be in dropdown menu.
- 'Second level horizontal', when you need 3 menu level support in navigation
bar. 1st and 2nd level will be standard menu items layout horizontally and 3th
menu level items will be in dropdown menu.


## Navbar off-canvas support (Experimental)

This theme can also support off-canvas navigation with a help of jasny/bootstrap
library. For more info visit [jansy/bootstrap examples](http://www.jasny.net/bootstrap/examples/navmenu)
page.

You first need to install jasny-bootstrap library as explained in bs_lib
README.md file - be sure to install forked version which is ported to Bootstrap
4.

Then in theme settings enable 'Navbar off-canvas type'.

__NOTES__
- Type `reveal` is currently not supported.
- Position `left` is not fully supported and tested. Position `top` and `bottom`
  are not supported at all for now.


## Language block settings

This theme supports two language block types in theme settings:
- 'Links' (default)
- 'Dropdown'

Language block type `links` is a standard Drupal language links rendering.

Language block type `dropdown` use Bootstrap dropdown button to render language
links.
You can override `links--dropdown-language-block.html.twig` template in your
custom theme to additionally change HTMl code of dropdown language block.

You can also control title of language link with 'Language block title' option:
- 'Label' - default, language name
- 'Language code' - use language code for link title


## Custom Forms

Enable 'Custom Bootstrap forms support' option in theme setting in order to use
[Bootstrap custom forms](https://v4-alpha.getbootstrap.com/components/forms/#custom-forms).

Checkboxes and radios, select menu and file browser custom forms are all
supported.



MISC
----

Additional documentation:

- See bs_base/README.md
- See starterkit_bootstrap/README.md for explanation how to easily create new
  child theme from starterkit template.
