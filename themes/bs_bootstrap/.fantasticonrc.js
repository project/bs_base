'use strict'

const codepoints = require('./fantasticon/icons.json')

module.exports = {
  // Required SVG image icons path.
  inputDir: './images/font-icons',
  // Required output font path.
  outputDir: './fonts',
  // Base name of the font set used both as default asset name.
  name: 'icon',
  // Which font types to generate.
  fontTypes: ['woff2'],
  // Which asset types to generate.
  assetTypes: ['scss', 'json'],
  codepoints,
  // Json format options.
  formatOptions: {
    json: {
      indent: 2
    }
  },
  // Use descent to fix vertical font centering when needed.
  // @see https://github.com/tancredi/fantasticon/issues/11
  descent: 55,
  // If the icons are different height use normalize to scale them to the height
  // of the highest icon.
  //normalize: true,
  // Handlebars templates for generation.
  templates: {
    scss: './fantasticon/scss.hbs'
  },
  // These are generated output paths configuration.
  pathOptions: {
    json: './fantasticon/icons.json',
    scss: './sass/variables/_icons.scss',
    woff2: './fonts/icons.woff2'
  }
}
