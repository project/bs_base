<?php

/**
 * @file
 * Update hooks for bs_bootstrap base themes.
 */

/**
 * Example update function example implementation of hook_bs_update_8xxx().
 */
/*function bs_bootstrap_bs_update_8xxx($target_theme_name) {
  // Get all existing themes information.
  $themes_info = _bs_base_drupal_theme_list_info();

  // Get child themes of passed parent theme.
  $child_themes = _bs_base_find_child_themes('bs_bootstrap');

  // Get first parent theme of passed child theme.
  $parent_theme_name = _bs_base_drupal_get_parent_theme_name($target_theme_name);

  // Access the targeted theme info only.
  $themes_info[$target_theme_name];

  // Loop through all child themes.
  foreach ($child_themes as $child_theme) {
    // Do something.
  }
}*/

use Drush\Drush;
use Psr\Log\LogLevel;

/**
 * Update gulp toolkit versions.
 */
function bs_bootstrap_bs_update_8000($target_theme_name) {
  $themes_info = _bs_base_drupal_theme_list_info();

  // Update Bootstrap version information in package.json.
  if (isset($themes_info[$target_theme_name])) {
    _bs_base_regexp_file($themes_info[$target_theme_name]->subpath . '/package.json', [
      '"autoprefixer": "\^7.2.3"' => '"autoprefixer": "^9.4.10"',
      '"css-mqpacker": "\^6.0.1"' => '"css-mqpacker": "^7.0.0"',
      '"deepmerge": "\^2.0.1"' => '"deepmerge": "^3.2.0"',
      '"del": "\^3.0.0"' => '"del": "^4.0.0"',
      '"eslint": "\^4.14.0"' => '"eslint": "^5.15.1"',
      '"gulp": "\^3.9.1"' => '"gulp": "^4.0.0"',
      '"gulp-cached": "\^1.1.0"' => '"gulp-cached": "^1.1.1"',
      '"gulp-changed": "\^3.1.1"' => '"gulp-changed": "^3.2.0"',
      '"gulp-concat": "\^2.6.0"' => '"gulp-concat": "^2.6.1"',
      '"gulp-if": "\^2.0.1"' => '"gulp-if": "^2.0.2"',
      '"gulp-load-plugins": "\^1.4.0"' => '"gulp-load-plugins": "^1.5.0"',
      '"gulp-notify": "\^3.0.0"' => '"gulp-notify": "^3.2.0"',
      '"gulp-plumber": "\^1.1.0"' => '"gulp-plumber": "^1.2.1"',
      '"gulp-postcss": "\^7.0.0"' => '"gulp-postcss": "^8.0.0"',
      '"gulp-sass": "\^3.1.0"' => '"gulp-sass": "^4.0.2"',
      '"gulp-sass-glob": "\^1.0.8"' => '"gulp-sass-glob": "^1.0.9"',
      '"gulp-sass-inject": "\^1.1.0"' => '"gulp-sass-inject": "^1.1.1"',
      '"gulp-sass-lint": "\^1.1.1"' => '"gulp-sass-lint": "^1.4.0"',
      '"gulp-sourcemaps": "~2.6.2"' => '"gulp-sourcemaps": "~2.6.5"',
      '"gulp-strip-css-comments": "\^1.2.0"' => '"gulp-strip-css-comments": "^2.0.0"',
      '"gulp-svgstore": "\^6.0.0"' => '"gulp-svgstore": "^7.0.1"',
      '"gulp-uglify": "\^3.0.0"' => '"gulp-uglify": "^3.0.2"',
      '"js-yaml": "\^3.8.1"' => '"js-yaml": "^3.12.2"',
      '"postcss-reporter": "\^5.0.0"' => '"postcss-reporter": "^6.0.1"',
      '"stylelint": "\^8.4.0"' => '"stylelint": "^9.10.1"',
      '"stylelint-config-standard": "\^18.0.0"' => '"stylelint-config-standard": "^18.2.0"',
      '"stylelint-order": "\^0.8.0"' => '"stylelint-order": "^2.1.0"',
      '"stylelint-scss": "\^2.2.0"' => '"stylelint-scss": "^3.5.4"',
    ]);
  }
}

/**
 * Update Bootstrap to 4.3.1.
 */
function bs_bootstrap_bs_update_8001($target_theme_name) {
  $themes_info = _bs_base_drupal_theme_list_info();

  // Update Bootstrap version information in package.json.
  if (isset($themes_info[$target_theme_name])) {
    _bs_base_regexp_file($themes_info[$target_theme_name]->subpath . '/package.json', [
      '"bootstrap": "~4.1.3"' => '"bootstrap": "~4.3.1"',
    ]);
  }
}

/**
 * Replace Autoprefixer browsers option to Browserslist config.
 */
function bs_bootstrap_bs_update_8002($target_theme_name) {
  $themes_info = _bs_base_drupal_theme_list_info();
  $parent_theme_name = _bs_base_drupal_get_parent_theme_name($target_theme_name);

  // Copy .browserslistrc file from first parent theme.
  $options = [
    'parent_path' => $themes_info[$parent_theme_name]->subpath,
    'child_path' => $themes_info[$target_theme_name]->subpath,
  ];
  if (_bs_base_copy_file('.browserslistrc', $options)) {
    drush_print('Successfully copied the .browserslistrc file from ' . $parent_theme_name . ' theme.', 2);
  }
  else {
    drush_print("WARNING: Could not copy the file .browserslistrc from $parent_theme_name theme. Maybe it already exist in $target_theme_name or it is missing in $parent_theme_name?", 2);
  }
}

/**
 * Make sure Bootstrap is updated to 4.3.1.
 */
function bs_bootstrap_bs_update_8003($target_theme_name) {
  $themes_info = _bs_base_drupal_theme_list_info();

  // Update Bootstrap version information in package.json.
  if (isset($themes_info[$target_theme_name])) {
    _bs_base_regexp_file($themes_info[$target_theme_name]->subpath . '/package.json', [
      '"bootstrap": ".*"' => '"bootstrap": "~4.3.1"',
    ]);
  }
}

/**
 * Bootstrap update to 4.6.0.
 */
function bs_bootstrap_bs_update_8004($target_theme_name) {
  $themes_info = _bs_base_drupal_theme_list_info();

  // Update Bootstrap version information in package.json.
  if (isset($themes_info[$target_theme_name])) {
    _bs_base_regexp_file($themes_info[$target_theme_name]->subpath . '/package.json', [
      '"bootstrap": ".*"' => '"bootstrap": "~4.6.0"',
    ]);
  }
}

/**
 * Bootstrap update to 4.6.1.
 */
function bs_bootstrap_bs_update_8005($target_theme_name) {
  $themes_info = _bs_base_drupal_theme_list_info();

  // Update Bootstrap version information in package.json.
  if (isset($themes_info[$target_theme_name])) {
    _bs_base_regexp_file($themes_info[$target_theme_name]->subpath . '/package.json', [
      '"bootstrap": ".*"' => '"bootstrap": "~4.6.1"',
    ]);
  }
}

/**
 * Add links.css to global css library.
 */
function bs_bootstrap_bs_update_8006($target_theme_name) {
  $themes_info = _bs_base_drupal_theme_list_info();
  if (isset($themes_info[$target_theme_name])) {
    _bs_base_regexp_file($themes_info[$target_theme_name]->subpath . '/' . $target_theme_name . '.libraries.yml', [
      'css/components/entity.css: {\s*}' => "css/components/entity.css: {}\n      css/components/links.css: {}",
    ]);
  }
}

/**
 * Add support for icon font generation.
 */
function bs_bootstrap_bs_update_8007($target_theme_name) {
  $themes_info = _bs_base_drupal_theme_list_info();
  if (isset($themes_info[$target_theme_name])) {
    // package.json changes.
    _bs_base_regexp_file($themes_info[$target_theme_name]->subpath . '/package.json', [
      // Add new packages.
      '"autoprefixer": ".*"' => "\"@twbs/fantasticon\": \"^2.7.1\",\n    \"autoprefixer\": \"^10.4.14\"",
      '"node-sass": ".*"' => "\"node-sass\": \"^8.0.0\",\n    \"npm-run-all\": \"^4.1.5\",\n    \"picocolors\": \"^1.0.0\"",
      // Add new build commands.
      '"build-css":.*"' => "\"build-install\": \"((type pnpm && pnpm install) || npm install)\",\n    \"build-browsers\": \"npx browserslist@latest --update-db\",\n    \"build-css\": \"npx gulp clean:css && npx gulp sass\",\n    \"build-icons\": \"fantasticon\",\n    \"build\": \"npm-run-all build-install build-icons build-browsers build-css\"",
    ]);

    // Add icons.css to theme global-styling library definition.
    _bs_base_regexp_file($themes_info[$target_theme_name]->subpath . '/' . $target_theme_name . '.libraries.yml', [
      'css/components/entity.css: {\s*}' => "css/components/entity.css: {}\n      css/components/icons.css: {}",
    ]);

    // Copy files needed for icons build from parent theme.
    $parent_theme_name = $themes_info[$target_theme_name]->info['base theme'];
    $options = [
      'parent_machine_name' => $parent_theme_name,
      'parent_path' => _bs_base_drupal_get_theme_path($parent_theme_name),
      'child_machine_name' => $target_theme_name,
      'child_path' => _bs_base_drupal_get_theme_path($target_theme_name),
    ];
    foreach (['fonts', '.fantasticonrc.js'] as $file) {
      _bs_base_copy_file($file, $options);
    }
    foreach (['fantasticon', 'images/font-icons'] as $folder) {
      _bs_base_recursive_copy($folder, $options);
    }

    // Add variables/icons import to init.
    // This will not enable new icons in updated theme but just allow building
    // of related icons font CSS.
    $init_path = $themes_info[$target_theme_name]->subpath . '/sass/_init.scss';
    if (_bs_base_regexp_exist($init_path, "@import \"variables/$target_theme_name\";")) {
      _bs_base_regexp_file($init_path, [
        "@import \"variables/$target_theme_name\";" => "@import \"variables/icons\";\n@import \"variables/$target_theme_name\";",
      ]);
    }
    else {
      Drush::logger()->log(LogLevel::ERROR, "Failed to update $init_path because it is missing '@import \"variables/$target_theme_name\";', you will need to manually update it and add '@import \"variables/icons\"'");
    }

  }
}

/**
 * Do not use npm-run-all.
 */
function bs_bootstrap_bs_update_8008($target_theme_name) {
  $themes_info = _bs_base_drupal_theme_list_info();
  if (isset($themes_info[$target_theme_name])) {
    // package.json changes.
    _bs_base_regexp_file($themes_info[$target_theme_name]->subpath . '/package.json', [
      // Remove npm-run-all.
      '^.*"npm-run-all": ".*",\n' => "",
      // Change build command.
      '"npm-run-all build-install build-icons build-browsers build-css"' => '"npm run build-install & npm run build-icons &npm run build-browsers & npm run build-css"',
    ]);
  }
}

/**
 * Add build-css-dev command to package.json.
 */
function bs_bootstrap_bs_update_8009($target_theme_name) {
  $themes_info = _bs_base_drupal_theme_list_info();
  if (isset($themes_info[$target_theme_name])) {
    _bs_base_regexp_file($themes_info[$target_theme_name]->subpath . '/package.json', [
      '^.*"build-css": ".*",\n' => '    "build-css": "npx gulp clean:css && npx gulp sass",' . "\n" . '    "build-css-dev": "npx gulp clean:css && npx gulp sass:dev",' . "\n",
    ]);
  }
}
