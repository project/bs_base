// Define gulp tasks.
module.exports = function (gulp, sass, plugins, options) {

  'use strict';

  const fs = require('fs');

  // Processor options.
  options.processors = [
    plugins.reporter(options.processors.reporter)
  ];

  // Post CSS options.
  options.postcssOptions = [
    plugins.autoprefixer(options.autoprefixer),
    plugins.hail2u.cssMqpacker({sort: true})
  ];

  // Defining gulp tasks.

  // Load theme _functions.scss partial if it exists, so we can use them
  // in theme-options.yml.
  // Clone themes array from parentTheme and add current theme because we need
  // to check it also.
  var allThemes = options.parentTheme ? JSON.parse(JSON.stringify(options.parentTheme)) : [];
  allThemes.push({name: 'current', path: './'});
  function loadFunctions(chunk, enc, callback) {
    for (var i = 0; i < allThemes.length; ++i) {
      var path = allThemes[i].path + 'sass/_functions.scss';
      if (fs.existsSync(path)) {
        var content = fs.readFileSync(path);
        if (content) {
          // We are injecting to the start of the buffer so functions are
          // before SASS vars that are coming from yaml.
          chunk.contents = Buffer.concat([content, chunk.contents], content.length + chunk.contents.length);
        }
      }
    }

    callback(null, chunk);
  }

  gulp.task('sass', function () {
    const task = gulp.src(options.sass.src + '/**/*.scss');

    task.pipe(plugins.sassInject(options.sass.injectVariables))
      .pipe(plugins.through.obj(loadFunctions))
      .pipe(sass({
        outputStyle: 'expanded',
        includePaths: options.sass.includePaths
      }).on('error', sass.logError))
      .pipe(plugins.postcss(options.postcssOptions))
      .pipe(plugins.stripCssComments())
      .pipe(gulp.dest(options.sass.dest));
    return task;
  });

  gulp.task('sass:dev', function () {
    const task = gulp.src(options.sass.src + '/**/*.scss', {sourcemaps: true});
    task.pipe(plugins.sassInject(options.sass.injectVariables))
      .pipe(plugins.through.obj(loadFunctions))
      .pipe(sass({
        outputStyle: 'expanded',
        includePaths: options.sass.includePaths,
      }).on('error', sass.logError))
      .pipe(plugins.debug())
      .pipe(plugins.postcss(options.postcssOptions))
      .pipe(gulp.dest(options.sass.dest, {sourcemaps: '.'}));
    return task;
  });

  gulp.task('clean:css', function () {
    return plugins.del([
      'css/**/*'
    ]);
  });

  // Gulp run all tasks.

  gulp.task('default', gulp.series('clean:css', 'sass'));
  gulp.task('prod', gulp.series('default'));
  gulp.task('dev', gulp.series('sass:dev'));
  gulp.task('clean', gulp.series('clean:css'));

  // Gulp watches.

  gulp.task('watch', function () {
    gulp.watch(options.sass.src + '/**/*.scss', ['sass']);
  });
  gulp.task('watch:dev', function () {
    gulp.watch(options.sass.src + '/**/*.scss', ['sass:dev']);
  });

};
